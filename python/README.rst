Python bindings for the Littlewood-Richardson Calculator
========================================================

This package provides Python bindings for the
`Littlewood-Richardson Calculator library
<https://math.rutgers.edu/~asbuch/lrcalc>`_.


Example
-------

.. code:: python

    >>> import lrcalc
    >>> lrcalc.mult([2,1], [2])
    {(2, 2, 1): 1, (4, 1): 1, (3, 1, 1): 1, (3, 2): 1}
    >>> lrcalc.skew([3,2,1], [2,1])
    {(2, 1): 2, (1, 1, 1): 1, (3,): 1}


Installation
------------

You need the LR Calculator library version 2 to be already installed on your
system, follow the instructions `here
<https://math.rutgers.edu/~asbuch/lrcalc>`_. Once this is done, the Python
bindings can be installed using pip::

    $ pip install lrcalc


Source
------

You can find the latest version of the source code on bitbucket:
https://bitbucket.org/asbuch/lrcalc.


License
-------

This package is distributed under the terms of the GNU General Public License
(GPL) published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version. See
http://www.gnu.org/licenses/.
