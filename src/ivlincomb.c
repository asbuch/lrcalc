/*  Littlewood-Richardson Calculator
 *  Copyright (C) 1999- Anders S. Buch (asbuch at math rutgers edu)
 *  See the file LICENSE for license information.
 */

#include "alloc.h"
#include "ivector.h"

#define _IVLINCOMB_C
#include "ivlincomb.h"

#include "hashtab.tpl.c"
